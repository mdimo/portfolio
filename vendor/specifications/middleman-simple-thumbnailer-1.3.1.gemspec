# -*- encoding: utf-8 -*-
# stub: middleman-simple-thumbnailer 1.3.1 ruby lib

Gem::Specification.new do |s|
  s.name = "middleman-simple-thumbnailer".freeze
  s.version = "1.3.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Jakub Niewczas".freeze]
  s.date = "2017-09-01"
  s.description = "Middleman extension that allows you to create image thumbnails by providing resize_to option to image_tag helper".freeze
  s.email = ["niewczas.jakub@gmail.com".freeze]
  s.homepage = "https://github.com/kubenstein/middleman-simple-thumbnailer".freeze
  s.licenses = ["MIT".freeze]
  s.rubygems_version = "2.7.7".freeze
  s.summary = "Middleman extension that allows you to create image thumbnails".freeze

  s.installed_by_version = "2.7.7" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<middleman-core>.freeze, ["~> 4"])
      s.add_runtime_dependency(%q<mini_magick>.freeze, ["~> 4"])
      s.add_development_dependency(%q<middleman-cli>.freeze, ["~> 4"])
      s.add_development_dependency(%q<rake>.freeze, ["~> 12"])
      s.add_development_dependency(%q<aruba>.freeze, ["~> 0.14.2"])
      s.add_development_dependency(%q<cucumber>.freeze, ["~> 2.4"])
      s.add_development_dependency(%q<capybara>.freeze, ["~> 2.15"])
    else
      s.add_dependency(%q<middleman-core>.freeze, ["~> 4"])
      s.add_dependency(%q<mini_magick>.freeze, ["~> 4"])
      s.add_dependency(%q<middleman-cli>.freeze, ["~> 4"])
      s.add_dependency(%q<rake>.freeze, ["~> 12"])
      s.add_dependency(%q<aruba>.freeze, ["~> 0.14.2"])
      s.add_dependency(%q<cucumber>.freeze, ["~> 2.4"])
      s.add_dependency(%q<capybara>.freeze, ["~> 2.15"])
    end
  else
    s.add_dependency(%q<middleman-core>.freeze, ["~> 4"])
    s.add_dependency(%q<mini_magick>.freeze, ["~> 4"])
    s.add_dependency(%q<middleman-cli>.freeze, ["~> 4"])
    s.add_dependency(%q<rake>.freeze, ["~> 12"])
    s.add_dependency(%q<aruba>.freeze, ["~> 0.14.2"])
    s.add_dependency(%q<cucumber>.freeze, ["~> 2.4"])
    s.add_dependency(%q<capybara>.freeze, ["~> 2.15"])
  end
end
